
# testGraphQL

----
## Ejemplos de peticiones

* Crear jugador

> 
    mutation {
        createPlayer (
			player: {
				name: "Prueba"
				surname: "1"
				nationality: "Spain"
				position: "DC"
				birthdate: "1970-01-01T00:00:00"
	        }
        ) {
			id
			name
			surname
	    }
    }

* Buscar liga con equipos
>
    query {
        allLeagues {
			id
			name
			teams {
				id
				name
				stadium
	        }
	    }
    }
	
* Buscar jugador con equipo
>
    {
        allPlayers {
			id
			name
			surname
			position
			team {
				id
				name
			}
	    }
    }

* Modificar jugador
>
    mutation {
        modifyPlayer (player: 1, team: 999) {
			id
			name
			surname
			position
			team {
				id
				name
			}
	    }
    }
	
* Eliminar jugador
>
    mutation {
        removePlayer(player: 1) {
			id
			name
			surname
			position
			team {
				id
				name
			}
	    }
    }