package com.cjgmj.testGraphQL.service;

import java.util.List;

import com.cjgmj.testGraphQL.entity.LeagueEntity;
import com.cjgmj.testGraphQL.objects.type.League;

public interface LeagueService {

	public List<LeagueEntity> listAllLeagues();

	public List<League> getLeagues();

}
