package com.cjgmj.testGraphQL.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cjgmj.testGraphQL.entity.TeamEntity;
import com.cjgmj.testGraphQL.repository.TeamRepository;
import com.cjgmj.testGraphQL.service.TeamService;

@Service
public class TeamServiceImpl implements TeamService {

	@Autowired
	private TeamRepository teamRepository;

	@Override
	public List<TeamEntity> listAllTeams() {
		return teamRepository.findAll();
	}

}
