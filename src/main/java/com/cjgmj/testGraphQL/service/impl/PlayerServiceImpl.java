package com.cjgmj.testGraphQL.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cjgmj.testGraphQL.entity.PlayerEntity;
import com.cjgmj.testGraphQL.entity.TeamEntity;
import com.cjgmj.testGraphQL.mapper.PlayerMapper;
import com.cjgmj.testGraphQL.objects.input.PlayerInput;
import com.cjgmj.testGraphQL.objects.type.Player;
import com.cjgmj.testGraphQL.repository.PlayerRepository;
import com.cjgmj.testGraphQL.repository.TeamRepository;
import com.cjgmj.testGraphQL.service.PlayerService;

@Service
public class PlayerServiceImpl implements PlayerService {

	@Autowired
	private PlayerRepository playerRepository;

	@Autowired
	private TeamRepository teamRepository;

	@Autowired
	private PlayerMapper playerMapper;

	@Override
	@Transactional(readOnly = true)
	public List<Player> listAllPlayers() {
		return playerMapper.playersEntityToPlayers(playerRepository.findAll());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Player> findTop5ByNationality(String nationality) {
		return playerMapper.playersEntityToPlayers(playerRepository.findTop5ByNationality(nationality));
	}

	@Override
	@Transactional(readOnly = true)
	public Player findFirstByLeague(String league) {
		return playerMapper.playerEntityToPlayer(playerRepository.findFirstByLeague(league));
	}

	@Override
	public Player savePlayer(PlayerInput playerInput) {
		PlayerEntity player = playerMapper.playerInputToPlayerEntity(playerInput);
		return playerMapper.playerEntityToPlayer(playerRepository.save(player));
	}

	@Override
	@Transactional
	public Player updatePlayer(Long player, Long team) {
		PlayerEntity p = playerRepository.findById(player).orElse(null);
		TeamEntity t = teamRepository.findById(team).orElse(null);

		if (p != null && t != null) {
			p.setTeam(t);

			return playerMapper.playerEntityToPlayer(playerRepository.save(p));
		}

		return null;
	}

	@Override
	public Player removePlayer(Long player) {
		PlayerEntity p = playerRepository.findById(player).orElse(null);
		playerRepository.deleteById(player);

		return playerMapper.playerEntityToPlayer(p);
	}

}
