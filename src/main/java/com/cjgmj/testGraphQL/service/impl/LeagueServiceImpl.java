package com.cjgmj.testGraphQL.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cjgmj.testGraphQL.entity.LeagueEntity;
import com.cjgmj.testGraphQL.mapper.LeagueMapper;
import com.cjgmj.testGraphQL.objects.type.League;
import com.cjgmj.testGraphQL.repository.LeagueRepository;
import com.cjgmj.testGraphQL.service.LeagueService;

@Service
public class LeagueServiceImpl implements LeagueService {

	@Autowired
	private LeagueRepository leagueRepository;

	@Autowired
	private LeagueMapper leagueMapper;

	@Override
	public List<LeagueEntity> listAllLeagues() {
		return leagueRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<League> getLeagues() {
		return leagueMapper.leaguesEntityToLeagues(leagueRepository.findAll());
	}

}
