package com.cjgmj.testGraphQL.service;

import java.util.List;

import com.cjgmj.testGraphQL.entity.TeamEntity;

public interface TeamService {

	public List<TeamEntity> listAllTeams();

}
