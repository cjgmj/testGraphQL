package com.cjgmj.testGraphQL.service;

import java.util.List;

import com.cjgmj.testGraphQL.objects.input.PlayerInput;
import com.cjgmj.testGraphQL.objects.type.Player;

public interface PlayerService {

	public List<Player> listAllPlayers();

	public List<Player> findTop5ByNationality(String nationality);

	public Player findFirstByLeague(String league);

	public Player savePlayer(PlayerInput playerInput);

	public Player updatePlayer(Long player, Long team);

	public Player removePlayer(Long player);

}
