package com.cjgmj.testGraphQL.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cjgmj.testGraphQL.entity.PlayerEntity;

@Repository
public interface PlayerRepository extends JpaRepository<PlayerEntity, Long> {

	List<PlayerEntity> findTop5ByNationality(String nationality);

	@Query(value = "SELECT * FROM player p INNER JOIN team t ON p.team=t.id INNER JOIN league l ON t.league=l.id "
			+ "WHERE l.name=:league ORDER BY p.rate DESC, p.surname DESC LIMIT 1", nativeQuery = true)
	PlayerEntity findFirstByLeague(@Param("league") String league);

}
