package com.cjgmj.testGraphQL.mapper;

import org.mapstruct.Mapper;

import com.cjgmj.testGraphQL.entity.TeamEntity;
import com.cjgmj.testGraphQL.objects.type.Team;

@Mapper(componentModel = "spring")
public interface TeamMapper {

	Team teamEntityToTeam(TeamEntity team);

}
