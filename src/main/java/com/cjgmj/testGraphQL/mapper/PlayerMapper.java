package com.cjgmj.testGraphQL.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.cjgmj.testGraphQL.entity.LeagueEntity;
import com.cjgmj.testGraphQL.entity.PlayerEntity;
import com.cjgmj.testGraphQL.entity.TeamEntity;
import com.cjgmj.testGraphQL.objects.input.PlayerInput;
import com.cjgmj.testGraphQL.objects.type.League;
import com.cjgmj.testGraphQL.objects.type.Player;
import com.cjgmj.testGraphQL.objects.type.Team;

@Mapper(componentModel = "spring")
public interface PlayerMapper {

	@Mapping(target = "teams", ignore = true)
	League leagueMap(LeagueEntity leagueEntity);

	@Mapping(target = "players", ignore = true)
	Team teamMap(TeamEntity teamEntity);

	Player playerEntityToPlayer(PlayerEntity league);

	List<Player> playersEntityToPlayers(List<PlayerEntity> league);

	PlayerEntity playerInputToPlayerEntity(PlayerInput playerInput);
}
