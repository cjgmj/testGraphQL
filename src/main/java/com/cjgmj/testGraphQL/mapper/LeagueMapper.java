package com.cjgmj.testGraphQL.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.cjgmj.testGraphQL.entity.LeagueEntity;
import com.cjgmj.testGraphQL.entity.PlayerEntity;
import com.cjgmj.testGraphQL.entity.TeamEntity;
import com.cjgmj.testGraphQL.objects.type.League;
import com.cjgmj.testGraphQL.objects.type.Player;
import com.cjgmj.testGraphQL.objects.type.Team;

@Mapper(componentModel = "spring")
public interface LeagueMapper {

	@Mapping(target = "team", ignore = true)
	Player playerMap(PlayerEntity playerEntity);

	@Mapping(target = "league", ignore = true)
	Team teamMap(TeamEntity teamEntity);

	League leagueEntityToLeague(LeagueEntity leagueEntity);

	List<League> leaguesEntityToLeagues(List<LeagueEntity> league);

}
