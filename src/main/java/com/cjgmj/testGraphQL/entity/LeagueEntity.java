package com.cjgmj.testGraphQL.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name = "league", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class LeagueEntity {

	@Id
	@GeneratedValue
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "amountTeams", nullable = false)
	private int amountTeams;

	@Column(name = "actualChampion")
	private String actualChampion;

	@Column(name = "teamMoreTitles")
	private String teamMoreTitles;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "league")
	private List<TeamEntity> teams;

}
