package com.cjgmj.testGraphQL;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.coxautodev.graphql.tools.SchemaParserOptions;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

@Configuration
public class DateMapper {

	@Bean
	public SchemaParserOptions schemaParserOptions() {
		return SchemaParserOptions.newOptions().objectMapperConfigurer((mapper, context) -> {
			mapper.registerModule(new JavaTimeModule());
		}).build();
	}

}
