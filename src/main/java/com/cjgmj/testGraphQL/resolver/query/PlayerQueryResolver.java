package com.cjgmj.testGraphQL.resolver.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjgmj.testGraphQL.objects.type.Player;
import com.cjgmj.testGraphQL.service.PlayerService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class PlayerQueryResolver implements GraphQLQueryResolver {

	@Autowired
	private PlayerService playerService;

	public List<Player> allPlayers() {
		return playerService.listAllPlayers();
	}

}
