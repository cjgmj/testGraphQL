package com.cjgmj.testGraphQL.resolver.query;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjgmj.testGraphQL.objects.type.League;
import com.cjgmj.testGraphQL.service.LeagueService;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class LeagueQueryResolver implements GraphQLQueryResolver {

	@Autowired
	private LeagueService leagueService;

	public List<League> allLeagues() {
		return leagueService.getLeagues();
	}

}
