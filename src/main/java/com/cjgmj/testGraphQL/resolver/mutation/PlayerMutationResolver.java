package com.cjgmj.testGraphQL.resolver.mutation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cjgmj.testGraphQL.objects.input.PlayerInput;
import com.cjgmj.testGraphQL.objects.type.Player;
import com.cjgmj.testGraphQL.service.PlayerService;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;

@Component
public class PlayerMutationResolver implements GraphQLMutationResolver {

	@Autowired
	private PlayerService playerService;

	public Player createPlayer(PlayerInput playerI) {
		return playerService.savePlayer(playerI);
	}

	public Player modifyPlayer(Long player, Long team) {
		return playerService.updatePlayer(player, team);
	}

	public Player removePlayer(Long player) {
		return playerService.removePlayer(player);
	}

}
