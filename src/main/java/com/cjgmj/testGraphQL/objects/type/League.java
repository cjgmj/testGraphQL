package com.cjgmj.testGraphQL.objects.type;

import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class League implements Serializable {

	private static final long serialVersionUID = 5357953959972399209L;

	private Long id;
	private String name;
	private Integer amountTeams;
	private String actualChampion;
	private String teamMoreTitles;
	private List<Team> teams;

}
