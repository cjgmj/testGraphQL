package com.cjgmj.testGraphQL.objects.type;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Team implements Serializable {

	private static final long serialVersionUID = 8688748449488586380L;

	private Long id;
	private String name;
	private String stadium;
	private String president;
	private LocalDateTime dateFoundation;
	private String location;
	private List<Player> players;
	private League league;

}
