package com.cjgmj.testGraphQL.objects.type;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Player implements Serializable {

	private static final long serialVersionUID = -2654094981136529391L;

	private Long id;
	private String name;
	private String surname;
	private String nationality;
	private String position;
	private LocalDateTime birthdate;
	private String height;
	private Integer rate;
	private Team team;

}
