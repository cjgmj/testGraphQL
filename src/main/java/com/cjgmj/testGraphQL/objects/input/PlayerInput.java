package com.cjgmj.testGraphQL.objects.input;

import java.io.Serializable;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PlayerInput implements Serializable {

	private static final long serialVersionUID = -5347292551826883756L;

	private String name;
	private String surname;
	private String nationality;
	private String position;
	private LocalDateTime birthdate;

}
